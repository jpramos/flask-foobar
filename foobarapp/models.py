
from foobarapp import db

class Person(db.Model):
    __tablename__  = 'tbl_person'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    type = db.Column(db.String(32))
    availabilities = db.relationship('Availability', backref='owner',
            lazy='dynamic', cascade='all,delete')

    __mapper_args__ = {
            'polymorphic_on': type,
            'polymorphic_identity': 'person'
            }

class Employee(Person):

    __mapper_args__ = {
        'polymorphic_identity': 'employee'
        }

    def __repr__(self):
        return '<Employee {}>'.format(self.name)

class Candidate(Person):

    __mapper_args__ = {
        'polymorphic_identity': 'candidate'
        }

    def __repr__(self):
        return '<Candidate {}>'.format(self.name)


class Availability(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    person_id = db.Column(db.Integer, db.ForeignKey('tbl_person.id'))
    start_period = db.Column(db.DateTime)
    end_period = db.Column(db.DateTime)

    def __repr__(self):
        return '<User {} is available between {} and {}>'.format(
                self.person_id,
                self.start_period, 
                self.end_period)


