
from foobarapp import app

@app.route('/')
@app.route('/index')
def index():
    return 'Foo bar Application'
