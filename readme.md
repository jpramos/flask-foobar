
## Synopsis

REST interfaces to manage interviews

## How to Run

Build Container
- % docker build -t foobar:latest .

Run Container
- % docker run -e FLASK_APP=foobar.py --name foobar -p 5000:5000 foobar

## Examples

a. Manage a list of employees who are making the interviews
    Create an employee
    - % curl http://0.0.0.0:5000/employees -d "name=John Doe" -X POST -v

    Delete an employee
    - % curl http://0.0.0.0:5000/employee/<employee_id> -X DELETE -v

    list employees
    - % curl http://0.0.0.0:5000/employees -X GET -v

b. Manage a list of potential candidates
    Create an candidate
    - % curl http://0.0.0.0:5000/candidates -d "name=John Doe" -X POST -v

    Delete an candidate
    - % curl http://0.0.0.0:5000/candidate/<employee_id> -X DELETE -v

    list employees
    - % curl http://0.0.0.0:5000/employees -X GET -v


c. Submit availability for the employee/candidate for the next week (that means, the list of
days of the week / hours available for the interview). For simplicity, we assume that we
always deal with one specific week only (in other words, on Friday evening the assistant is
making plans for the coming week). But you’re welcome to extend the task!

    Add availability
    - % curl http://0.0.0.0:5000/availabilities -d
    "person_id=1&start_period=2018-02-26 15&end_period=2018-02-26 18" -X POST
    -v

    update availability
    - % curl http://0.0.0.0:5000/availability/3 -d "start_period=2018-02-16 15&end_period=2018-02-16 17" -X PUT -v

d. Receive a list of time slots to make an interview (providing a candidate and list of
interviewers required). If there is no time slot available during next week, the service
should return an appropriate error.

    - % curl http://0.0.0.0:5000/match_availability/2 -d "employee_list=1" -X GET -v


## Unit Testing

In the top-level folder:
    % nosetests
