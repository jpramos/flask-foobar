FROM python:3

MAINTAINER Joao P. Ramos "ramosjp@gmail.com"


#RUN python -m venv foobarenv

COPY . /app

WORKDIR /app

#RUN . /foobarenv/bin/activate; pip install -r requirements.txt
RUN pip install -r requirements.txt

EXPOSE 5000

CMD [".", "/foobarenv/bin/activate"]
CMD [ "python", "-m", "flask", "run", "--host=0.0.0.0" ]
