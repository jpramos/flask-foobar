
from foobarapp import app, db, models, api
from flask_restful import reqparse, Resource
from datetime import datetime
from flask_jsonpify import jsonify

class EmployeeResource(Resource):
    def get(self, employee_id):
        """
            get record from employee
        """

        employee = (db.session.query(models.Employee)
                .filter_by(id=employee_id).first_or_404())

        return {employee_id: employee.name}


    def delete(self, employee_id):
        """
            delete employee record
        """

        employee = (db.session.query(models.Employee)
                .filter_by(id=employee_id).first_or_404())

        db.session.delete(employee)
        db.session.commit()

        return {'message': 'employee deleted successfully'}, 200


class EmployeeListResource(Resource):
    def get(self):
        """ 
            get list of employees
        """

        return [{employee.id: {'name': employee.name}} for employee in
                db.session.query(models.Employee).all()]

    def post(self):
        """
            add an employee to the list
        """

        parser = reqparse.RequestParser()
        parser.add_argument('name')
        args = parser.parse_args()
        if not 'name' in args:
            return {'message': 'form is incomplete'}, 400
        new_employee = models.Employee(name=args['name'])
        db.session.add(new_employee)
        db.session.commit()

        return {new_employee.id: {'name': new_employee.name}}, 201


class CandidateResource(Resource):
    def get(self, candidate_id):
        """
            get the candidate's record
        """

        candidate = (db.session.query(models.Candidate)
                .filter_by(id=candidate_id).first_or_404())
        return {candidate_id: candidate}

    def delete(self, candidate_id):
        """
            delete candidate's record
        """

        candidate = (db.session.query(models.Candidate)
                .filter_by(id=candidate_id).first_or_404())

        db.session.delete(candidate)
        db.session.commit()

        return {'message': 'candidate deleted successfully'}, 200

class CandidateListResource(Resource):
    def get(self):
        """
            get list of candidates
        """

        return [{candidate.id: {'name': candidate.name}} for candidate in
                db.session.query(models.Candidate).all()]

    def post(self):
        """
            add a candidate to the list
        """

        parser = reqparse.RequestParser()
        parser.add_argument('name')
        args = parser.parse_args()
        if not 'name' in args:
            return {'message': 'form incomplete'}, 400
        new_candidate = models.Candidate(name=args['name'])
        db.session.add(new_candidate)
        db.session.commit()

        return {new_candidate.id: {'name': new_candidate.name}}, 201


class AvailabilityResource(Resource):
    def get(self, availability_id):
        """
            get the availability record
        """

        availability = (db.session.query(models.Availability)
                .filter_by(id=availability_id).first_or_404())
        return {availability_id: {'user_name': availability.owner.name, 
            'type': availability.owner.type,
            'start_period': str(availability.start_period),
            'end_period': str(availability.end_period)}}


    def delete(self, availability_id):
        """
            delete the availability record
        """

        availability = (db.session.query(models.Availability)
                .filter_by(id=availability_id).first_or_404())

        db.session.delete(availability)
        db.session.commit()

        return {'message': 'availability removed successfully'}, 200

    def put(self, availability_id):
        """
            update availability
        """

        parser = reqparse.RequestParser()
        parser.add_argument('start_period')
        parser.add_argument('end_period')
        args = parser.parse_args()

        availability = (db.session.query(models.Availability)
                .filter_by(id=availability_id).first_or_404())
        availability.start_period = datetime.strptime(args['start_period'],
                '%Y-%m-%d %H')
        availability.end_period = datetime.strptime(args['end_period'], 
                '%Y-%m-%d %H')

        if availability.start_period >= availability.end_period:
            return ({'message': 'start_period cannot be later than end_period'},
                    400)

        db.session.add(availability)
        db.session.commit()

        return {availability_id: {'user_name': availability.owner.name, 
            'type': availability.owner.type,
            'start_period': str(availability.start_period),
            'end_period': str(availability.end_period)}}, 202

class AvailabilityListResource(Resource):
    def get(self):
        """
            get list of availabilities
        """

        return [{availability.id: {'person_name': availability.owner.name,
            'type': availability.owner.type, 
            'start_period': str(availability.start_period), 
            'end_period': str(availability.end_period)}}
            for availability in db.session.query(models.Availability).all()]


    def post(self):
        """
            add an availability to the list
        """

        parser = reqparse.RequestParser()
        parser.add_argument('person_id')
        parser.add_argument('start_period')
        parser.add_argument('end_period')
        args = parser.parse_args()
        print(args)

        if (not 'person_id' in args or not 'start_period' in args or not 'end_period'
                in args):
            return {'message': 'all fields are required'}, 400

        person = db.session.query(models.Person).filter_by(id=args['person_id']).first()
        if person is None:
            return {'message': 'This user is not in the system'}, 400

        new_availability = models.Availability(person_id=args['person_id'],
                start_period=datetime.strptime(
                    args['start_period'], '%Y-%m-%d %H'),
                end_period=datetime.strptime(
                    args['end_period'], '%Y-%m-%d %H'))

        if new_availability.start_period >= new_availability.end_period:
            return ({'message': 'start_period cannot be later than end_period'}, 400)

        db.session.add(new_availability)
        db.session.commit()

        return {new_availability.id: {'user_name': new_availability.owner.name, 
            'type': new_availability.owner.type,
            'start_period': str(new_availability.start_period),
            'end_period': str(new_availability.end_period)}}, 201



class MatchAvailabilityResource(Resource):
    def get(self, candidate_id):
        """
            get the employees' availability for a given candidate
        """

        parser = reqparse.RequestParser()
        parser.add_argument('employee_list')
        employee_list = parser.parse_args()['employee_list']

        employee_arr = [int(x) for x in employee_list.split(',')]

        employees = (db.session.query(models.Employee)
                .filter(models.Employee.id.in_(employee_arr)).all())

        candidate = (db.session.query(models.Candidate)
                .filter_by(id=candidate_id).first_or_404())

        cand_availabilities = candidate.availabilities.all()

        availabilities = {}
        for cand_avail in cand_availabilities:

            for employee in employees:

                emp_avail = (employee.availabilities
                        .filter(cand_avail.end_period >= models.Availability.start_period,
                            models.Availability.end_period >=
                            cand_avail.start_period).all())

                if len(emp_avail) != 0:
                    availabilities[employee.name] = [{'start_period':str(avail.start_period), 
                        'end_period': str(avail.end_period)} for
                        avail in emp_avail]
                    print(availabilities)

        if len(availabilities) == 0:
            return ({'message': 'No available period was found with this ' \
                'list of employees'}, 400)
        else:
            return availabilities




api.add_resource(EmployeeResource, '/employee/<employee_id>')
api.add_resource(EmployeeListResource, '/employees')

api.add_resource(CandidateResource, '/candidate/<candidate_id>')
api.add_resource(CandidateListResource, '/candidates')

api.add_resource(AvailabilityResource, '/availability/<availability_id>')
api.add_resource(AvailabilityListResource, '/availabilities')

api.add_resource(MatchAvailabilityResource,
        '/match_availability/<candidate_id>')
