

from foobarapp import app, db, models, api
import unittest
import tempfile
import os
import json

class FoobarTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()


    def tearDown(self):

        people = models.Person.query.all()
        for p in people:
            db.session.delete(p)
        
        av = models.Availability.query.all()
        for a in av:
            db.session.delete(a)

        db.session.commit()

    def test_employee_creation(self):
        res = self.app.post('/employees', data={'name': 'John Doe'})
        print(json.loads(res.data))
        self.assertEqual(res.status_code, 201)
        self.assertIn('John Doe', str(res.data))

    def test_employee_deletion(self):
        res = self.app.post('employees', data={'name': 'John Doe'})
        self.assertEqual(res.status_code, 201)
        res_json = json.loads(res.data)

        res = self.app.delete('/employee/'+list(res_json.keys())[0])

        self.assertEqual(res.status_code, 200)


    def test_candidate_creation(self):
        res = self.app.post('/candidates', data={'name': 'John Not so Doe'})
        self.assertEqual(res.status_code, 201)
        self.assertIn('John Not so Doe', str(res.data))

    def test_candidate_deletion(self):
        res = self.app.post('candidates', data={'name': 'John Not so Doe'})
        self.assertEqual(res.status_code, 201)

        res_json = json.loads(res.data)
        res = self.app.delete('/candidate/'+list(res_json.keys())[0])
        
        self.assertEqual(res.status_code, 200)


    def test_create_availability(self):
        res = self.app.post('/candidates', data={'name': 'John Doe'})
        self.assertEqual(res.status_code, 201)

        res_json = json.loads(res.data)

        res = self.app.post('/availabilities', 
                data={'person_id': list(res_json.keys())[0], 
                    'start_period': '2018-02-24 15',
                    'end_period': '2018-02-24 17'})

        self.assertEqual(res.status_code, 201)


    def test_match_availability(self):

        res = self.app.post('/employees', data={'name': 'John Doe'})
        self.assertEqual(res.status_code, 201)

        res_json = json.loads(res.data)
        emp_id = list(res_json.keys())[0]

        res = self.app.post('/availabilities',
                data={'person_id': emp_id,
                    'start_period': '2018-02-24 15',
                    'end_period': '2018-02-24 17'})
        self.assertEqual(res.status_code, 201)

        res = self.app.post('/candidates', data={'name': 'John Not so Doe'})
        self.assertEqual(res.status_code, 201)

        res_json = json.loads(res.data)
        cand_id = list(res_json.keys())[0]

        res = self.app.post('/availabilities', 
                data={'person_id': cand_id,
                    'start_period': '2018-02-24 15',
                    'end_period': '2018-02-24 16'})
        self.assertEqual(res.status_code, 201)

        res = self.app.get('/match_availability/'+cand_id,
                data={'employee_list': str(emp_id)})
        self.assertIn('John Doe', str(res.data))


    def test_multiplematch_availability(self):

        res = self.app.post('/employees', data={'name': 'John Doe'})
        self.assertEqual(res.status_code, 201)

        res_json = json.loads(res.data)
        emp_id = list(res_json.keys())[0]

        res = self.app.post('/availabilities',
                data={'person_id': emp_id,
                    'start_period': '2018-02-24 15',
                    'end_period': '2018-02-24 17'})
        self.assertEqual(res.status_code, 201)

        res = self.app.post('/employees', data={'name': 'John Doe, II'})
        self.assertEqual(res.status_code, 201)

        res_json = json.loads(res.data)
        emp2_id = list(res_json.keys())[0]

        res = self.app.post('/availabilities',
                data={'person_id': emp2_id,
                    'start_period': '2018-02-24 15',
                    'end_period': '2018-02-24 16'})
        self.assertEqual(res.status_code, 201)


        res = self.app.post('/candidates', data={'name': 'John Not so Doe'})
        self.assertEqual(res.status_code, 201)

        res_json = json.loads(res.data)
        cand_id = list(res_json.keys())[0]

        res = self.app.post('/availabilities', 
                data={'person_id': cand_id,
                    'start_period': '2018-02-24 15',
                    'end_period': '2018-02-24 16'})
        self.assertEqual(res.status_code, 201)

        res = self.app.get('/match_availability/'+cand_id,
                data={'employee_list': '{},{}'.format(str(emp_id), str(emp2_id))})
        self.assertIn('II', str(res.data))


    def test_mismatch_availability(self):

        res = self.app.post('/employees', data={'name': 'John Doe'})
        self.assertEqual(res.status_code, 201)

        res_json = json.loads(res.data)
        emp_id = list(res_json.keys())[0]

        res = self.app.post('/availabilities',
                data={'person_id': emp_id,
                    'start_period': '2018-02-24 15',
                    'end_period': '2018-02-24 17'})
        self.assertEqual(res.status_code, 201)

        res = self.app.post('/candidates', data={'name': 'John Not so Doe'})
        self.assertEqual(res.status_code, 201)

        res_json = json.loads(res.data)
        cand_id = list(res_json.keys())[0]

        res = self.app.post('/availabilities', 
                data={'person_id': cand_id,
                    'start_period': '2018-02-23 15',
                    'end_period': '2018-02-23 16'})
        self.assertEqual(res.status_code, 201)

        res = self.app.get('/match_availability/'+cand_id,
                data={'employee_list': str(emp_id)})
        self.assertIn('No available period', str(res.data))





if __name__ == '__main__':
    unittest.main()
